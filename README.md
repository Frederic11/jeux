Memory

https://gitlab.com/simplonlyon/promo12/projects/jeu-js



**Le jeux**

Jeux de mémorisation, le joueur voit pendant  5 secondes un écran avec 10 paires de motifs, il doit ensuite se souvenir des motifs, les retrouver avec le minimum de temps et de mouvement. L'écran lui indique le nombre de paires trouvées et le temps qui s'écoule. La partie se termine quand toute les paires sont retrouvées.



**La réalisation**

J'ai choisis le jeux du memory, car le jeux faisait appel à la logique et demandait quelques efforts.
Je m'interrogeais notemment sur la gestion des cliques sur les cases, comment comparer les cases, comment les retirer, changer d'images. C'est un jeux populaire avec beacoup d'exemples qui me permettaient aussi de me documenter en cas de blocage.



**User Story**

Les formateurs Simplon

Ils souhaitent que les élèves réalisent un projet : un jeux. Le choix du jeux est libre. Une date est donnée pour le rendu de l'exercice.

L'objectif étant que les élèves affinent leur technique de réalisation de traveau. Ils doivent utiliser leur connaissance (que ce soit le codage ou la réalisation de maquette). Mais c'est aussi un moyen pour que les élèves apprennent à s'organiser. Par exemple, ils doivent utiliser une Milestone pour apprendre à gerer les délais.

**Maquette**



https://wireframe.cc/N5nFat





